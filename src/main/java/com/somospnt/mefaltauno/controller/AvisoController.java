package com.somospnt.mefaltauno.controller;

import com.somospnt.mefaltauno.service.AvisoService;
import com.somospnt.mefaltauno.service.CategoriaService;
import com.somospnt.mefaltauno.service.LocalidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AvisoController {

    @Autowired
    private AvisoService avisoService;

    @Autowired
    private LocalidadService localidadService;

    @Autowired
    private CategoriaService categoriaService;

    @RequestMapping("/crear-aviso")
    public String crearAviso(Model model) {
        model.addAttribute("localidades", localidadService.obtenerTodas());
        model.addAttribute("categorias", categoriaService.obtenerTodas());
        return "crearAviso";
    }

}
