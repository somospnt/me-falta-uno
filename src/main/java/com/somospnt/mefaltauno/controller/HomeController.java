package com.somospnt.mefaltauno.controller;

import com.somospnt.mefaltauno.service.AvisoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @Autowired
    private AvisoService avisoService;

    @RequestMapping("/home")
    public String home(Model model) {
        model.addAttribute("avisos", avisoService.buscarTodos());
        return "aviso";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/registro")
    public String registro() {
        return "registro";
    }

}
