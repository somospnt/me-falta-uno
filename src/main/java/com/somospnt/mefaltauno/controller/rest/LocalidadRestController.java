package com.somospnt.mefaltauno.controller.rest;

import com.somospnt.mefaltauno.domain.Localidad;
import com.somospnt.mefaltauno.service.LocalidadService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/localidad")
public class LocalidadRestController {

    @Autowired
    LocalidadService localidadService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Localidad> obtenerTodas() {
        return localidadService.obtenerTodas();
    }
}
