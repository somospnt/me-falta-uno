package com.somospnt.mefaltauno.controller.rest;

import com.somospnt.mefaltauno.service.UsuarioService;
import com.somospnt.mefaltauno.vo.UsuarioVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/usuario")
public class UsuarioRestController {
    
    @Autowired
    UsuarioService usuarioService;
    
    @RequestMapping(value="/registrar", method = RequestMethod.POST, consumes = "application/json")
    public void registrar(@RequestBody UsuarioVO usuario){
        usuarioService.registrar(usuario);
    }
}