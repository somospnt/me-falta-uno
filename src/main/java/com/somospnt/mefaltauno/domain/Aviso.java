package com.somospnt.mefaltauno.domain;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "AVISO")
public class Aviso {

    @Id
    private Long id;

    private String titulo;

    private String descripcion;

    private Integer cupo;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEvento;

    @ManyToOne
    @JoinColumn(name = "id_categoria", referencedColumnName = "id")
    private Categoria categoria;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "TAG_AVISO",
            joinColumns = {
                @JoinColumn(name = "id_aviso", referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_tag", referencedColumnName = "id")})
    private List<Tag> tags;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "USUARIO_AVISO",
            joinColumns = {
                @JoinColumn(name = "id_aviso", referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_usuario", referencedColumnName = "id")})
    private List<Usuario> participantes;

    @ManyToOne
    @JoinColumn(name = "id_localidad", referencedColumnName = "id")
    private Localidad localidad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaEvento() {
        return fechaEvento;
    }

    public void setFechaEvento(Date fechaEvento) {
        this.fechaEvento = fechaEvento;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Usuario> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(List<Usuario> participantes) {
        this.participantes = participantes;
    }

    public Localidad getLocalidad() {
        return localidad;
    }

    public void setLocalidad(Localidad ubicacion) {
        this.localidad = ubicacion;
    }

    public Integer getCupo() {
        return cupo;
    }

    public void setCupo(Integer cupo) {
        this.cupo = cupo;
    }

}
