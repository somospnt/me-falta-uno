package com.somospnt.mefaltauno.repository;

import com.somospnt.mefaltauno.domain.Aviso;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AvisoRepository extends JpaRepository<Aviso, Long> {

}
