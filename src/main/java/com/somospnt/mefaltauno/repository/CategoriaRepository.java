package com.somospnt.mefaltauno.repository;

import com.somospnt.mefaltauno.domain.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CategoriaRepository extends JpaRepository<Categoria, Long>{
    
    Categoria findByNombre(String nombre);

}
