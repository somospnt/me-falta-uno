package com.somospnt.mefaltauno.repository;

import com.somospnt.mefaltauno.domain.Localidad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocalidadRepository extends JpaRepository<Localidad, Integer> {

    Localidad findByNombre(String nombre);
}
