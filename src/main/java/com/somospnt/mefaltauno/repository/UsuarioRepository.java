package com.somospnt.mefaltauno.repository;

import com.somospnt.mefaltauno.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Usuario findByUsername(String nombre);

}
