package com.somospnt.mefaltauno.service;

import com.somospnt.mefaltauno.domain.Aviso;
import java.util.List;

public interface AvisoService {

    List<Aviso> buscarTodos();

    void crear(Aviso aviso);
}
