
package com.somospnt.mefaltauno.service;

import com.somospnt.mefaltauno.domain.Categoria;
import java.util.List;


public interface CategoriaService {

    List<Categoria> obtenerTodas();
    
}
