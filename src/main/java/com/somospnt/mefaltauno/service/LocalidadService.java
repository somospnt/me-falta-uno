package com.somospnt.mefaltauno.service;

import com.somospnt.mefaltauno.domain.Localidad;
import java.util.List;

public interface LocalidadService {

    List<Localidad> obtenerTodas();
}
