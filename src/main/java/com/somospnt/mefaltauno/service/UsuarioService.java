package com.somospnt.mefaltauno.service;

import com.somospnt.mefaltauno.domain.Usuario;
import com.somospnt.mefaltauno.vo.UsuarioVO;

public interface UsuarioService {

    Usuario buscarPorUsername(String nombre);

    void registrar(UsuarioVO usuarioVO);
}
