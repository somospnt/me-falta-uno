package com.somospnt.mefaltauno.service.impl;

import com.somospnt.mefaltauno.domain.Aviso;
import com.somospnt.mefaltauno.repository.AvisoRepository;
import com.somospnt.mefaltauno.repository.UsuarioRepository;
import com.somospnt.mefaltauno.service.AvisoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AvisoServiceImpl implements AvisoService {

    @Autowired
    private AvisoRepository avisoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public List<Aviso> buscarTodos() {
        return avisoRepository.findAll();
    }

    @Override
    public void crear(Aviso aviso) {
        avisoRepository.save(aviso);
    }

}
