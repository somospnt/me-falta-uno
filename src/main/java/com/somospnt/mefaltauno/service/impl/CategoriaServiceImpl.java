
package com.somospnt.mefaltauno.service.impl;

import com.somospnt.mefaltauno.domain.Categoria;
import com.somospnt.mefaltauno.repository.CategoriaRepository;
import com.somospnt.mefaltauno.service.CategoriaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoriaServiceImpl implements CategoriaService{

    @Autowired
    private CategoriaRepository categoriaRepository;
    
    @Override
    public List<Categoria> obtenerTodas() {
        return categoriaRepository.findAll();
    }
    
}
