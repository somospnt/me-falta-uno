package com.somospnt.mefaltauno.service.impl;

import com.somospnt.mefaltauno.domain.Localidad;
import com.somospnt.mefaltauno.repository.LocalidadRepository;
import com.somospnt.mefaltauno.service.LocalidadService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocalidadServiceImpl implements LocalidadService {

    @Autowired
    private LocalidadRepository localidadRepository;

    @Override
    public List<Localidad> obtenerTodas() {
        return localidadRepository.findAll();
    }

}
