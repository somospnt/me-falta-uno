package com.somospnt.mefaltauno.service.impl;

import com.somospnt.mefaltauno.domain.Usuario;
import com.somospnt.mefaltauno.repository.LocalidadRepository;
import com.somospnt.mefaltauno.repository.UsuarioRepository;
import com.somospnt.mefaltauno.service.UsuarioService;
import com.somospnt.mefaltauno.vo.UsuarioVO;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private LocalidadRepository localidadRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Usuario buscarPorUsername(String nombre) {
        if (nombre == null) {
            throw new IllegalArgumentException();
        } else {
            Pattern patron = Pattern.compile("[ ()$^*+?¿,]");
            Matcher match = patron.matcher(nombre);
            if (!match.find()) {
                return usuarioRepository.findByUsername(nombre);
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    @Override
    public void registrar(UsuarioVO usuarioVO) {
        if (usuarioVO == null) {
            throw new IllegalArgumentException();
        } else {
            Usuario nuevoUsuario = new Usuario();
            nuevoUsuario.setUsername(usuarioVO.getNombre());
            nuevoUsuario.setPassword(passwordEncoder.encode(usuarioVO.getPassword()));
            nuevoUsuario.setEmail(usuarioVO.getEmail());
            nuevoUsuario.setLocalidad(localidadRepository.findByNombre(usuarioVO.getNombreLocalidad()));
            usuarioRepository.save(nuevoUsuario);
        }
    }

}
