<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>  
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Me falta uno</title>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/pnt-mefaltauno.css">

        <script src="js/lib/jquery-1.11.1.js" type="text/javascript"></script>
 
    </head>
    <body>
        <div class="pnt-mfu-menu">

            <nav class="pnt-mfu-nav">
                <c:set var="cookieMfu" value="${cookie['usuarioLogueado'].value}"/>
                <div class="pnt-mfu-contenedorLoginRegistro col-lg-2">
                    
                            <!--<button type="button" id="botonIngreso" class="btn btn-primary" data-toggle="modal" data-target="#modalLogin">
                                Ingresar
                            </button>-->

                            <a type="button" id="botonRegistro" class="btn btn-primary" href="registro">
                                Registrarse
                            </a>

                    
                </div>
                <div id="result"></div>
                <div class="col-lg-1 pnt-mfu-nombre-usuario">
                    ${cookieMfu}
                </div>
            </nav>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header pnt-mfu-titulo-pag">
                        <h1>ME FALTA UNO</h1>
                    </div>
                    <h2 class="pnt-mfu-titulo-seccion">Listado de avisos</h2>
                    <div id="avisos">                       
                        <c:forEach var="aviso" items="${avisos}">
                            <div class="panel panel-primary pnt-mfu-aviso">
                                <div class="panel-heading">
                                    <div class="row"> 
                                        <div class="col-md-9">
                                            <h3 class="panel-title">${aviso.titulo}</h3>
                                        </div>
                                        <div class="col-md-3">
                                            <h3 class="panel-title" align="right"> ${aviso.fechaEvento}</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="pnt-mfu-aviso-cuerpo panel-body">         
                                    <p>DESCRIPCION</p>
                                    ${aviso.descripcion}
                                </div>
                                <div class="panel-footer">
                                    <p>CUPO: <span class="pnt-mfu-aviso-cupo">${aviso.cupo}</span></p>
                                    <p>LOCALIDAD: ${aviso.localidad.nombre}</p>
                                    <p>¿Quienes van? 
                                        <c:forEach var="participante" items="${aviso.participantes}">
                                            <a href="#">${participante.username}</a> 
                                        </c:forEach>        
                                    </p>
                                    <p>CATEGORIA: ${aviso.categoria.nombre}</p>
                                    <div class="clearfix">
                                        <button type="button" align="right" class="btn btn-primary btn-lg pull-right">UNIRSE</button>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>                              
                    </div>
                </div>
            </div>
        </div>
        <%@include file="/WEB-INF/jsp/login.jsp" %>

    </body>

    <script src="js/lib/bootstrap.min.js" type="text/javascript"></script>

    <script src ="js/app/meFaltaUno.js" type="text/javascript"></script>
    <script src ="js/app/ui/ui.js" type="text/javascript"></script>
    <script src ="js/app/ui/avisos/avisos.js" type="text/javascript"></script>
    <script src="js/app/ui/registro/registro.js" type="text/javascript"></script>
    <script type="text/javascript">
        meFaltaUno.ui.avisos.init();
    </script>

</html>