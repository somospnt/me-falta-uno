<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/pnt-mefaltauno.css">
        <title>Crear aviso</title>
    </head>
    <body>
        <div class="container">
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="control-label">Titulo:</label>
                    <input type="text" class="form-control" id="titulo" placeholder="Ingresá un titulo">
                </div>
                <div class="form-group">
                    <label class="control-label">Descripcion:</label>
                    <textarea class="form-control pnt-mfu-text-area" id="descripcion" placeholder="Ingresá una descripcion"></textarea>
                </div>
            </form>
        </div>
    </body>
</html>
