<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Ingresar usando mi cuenta</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12 ">
                            <div class="somospnt-box-login">
                                <form class="form-horizontal" action="login-autenticacion" method="POST">
                                    <div class="form-group">
                                        <label for="inputUsuario" class="col-sm-2 control-label">Usuario</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="username" name="username" placeholder="Usuario">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputContraseña" class="col-sm-2 control-label">Contraseña</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Recordarme
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary">Sign in</button>
                                            <a class="somospnt-margen-izquierdo-link" href="" >Registrate!</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</html>

