<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/pnt-mefaltauno.css">
        <script src="js/lib/jquery-1.11.1.js" type="text/javascript"></script>

        <script src ="js/app/meFaltaUno.js" type="text/javascript"></script>
        <script src ="js/app/ui/ui.js" type="text/javascript"></script>
        <script src ="js/app/service/service.js" type="text/javascript"></script>

        <script src="js/app/service/localidad/localidad.js" type="text/javascript"></script>

        <script src="js/app/service/registro/registro.js" type="text/javascript"></script>

        <script src="js/app/ui/registro/registro.js" type="text/javascript"></script>
    </head>
    <body>

        <div class="row">
            <div class="col-xs-12 text-center">
                <h1>Registrate a Me Falta Uno y enterate cuando empezamos!</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 ">
                <div class="somospnt-box-login">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="inputUsuario" class="col-sm-2 control-label">Usuario</label>
                            <div class="col-sm-8">
                                <input type="text" autocomplete='off' class="form-control" required="true" id="username" name="username" placeholder="Usuario"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputContraseña" class="col-sm-2 control-label">Contraseña</label>
                            <div class="col-sm-8">
                                <input type="password" autocomplete='off' class="form-control" required="true" id="password" name="password" placeholder="Contraseña"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" autocomplete='off' id="email" name="email" required="true" placeholder="Email"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputLocalidad" class="col-sm-2 control-label" id="localidades">Localidad</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="selectLocalidad" name="selectLocalidad">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="button" class="btn btn-primary js-meFaltaUno-registrar">Registrarse</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>