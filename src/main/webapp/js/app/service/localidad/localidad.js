meFaltaUno.service.localidad = (function () {

    function obtenerLocalidad() {
        return $.ajax({
            type: "GET",
            url: 'api/localidad/',
            dataType: 'json',
            contentType: 'application/json'
        });
    }

    return {
        obtenerLocalidad: obtenerLocalidad
    };

})();