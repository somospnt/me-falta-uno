meFaltaUno.ui.avisos = (function () {

    function init() {
        $("document").ready(function () {
            formatearCupos();
        });
    }

    function formatearCupos() {
        $(".pnt-mfu-aviso-cupo").each(function (i) {
            if ($(this).text() === "") {
                $(this).text("ILIMITADO");
            }
        });
    }

    return{
        init: init
    };

})();
