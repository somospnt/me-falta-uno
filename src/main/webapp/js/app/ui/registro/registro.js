meFaltaUno.ui.registro = (function () {

    function init() {
        $(".js-meFaltaUno-registrar").on("click", registro);
        inicializarCombo();
    }

    function registro() {
        var usuario = {
            nombre: $("#username").val(),
            password: $("#password").val(),
            email: $("#email").val(),
            nombreLocalidad: $("#selectLocalidad").val()
        };
        $.ajax({
            type: "POST",
            url: 'api/usuario/registrar',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(usuario)
        });
    }

    function inicializarCombo() {
        meFaltaUno.service.localidad.obtenerLocalidad()
                .done(function (result) {
                    var cant = result.length;
                    for (var i = 0; i < cant; i++) {
                        $('#selectLocalidad')
                                .append($("<option></option>")
                                        .attr("value", result[i].nombre)
                                        .text(result[i].nombre));
                    }
                })
                .error(function () {
                    console.log("No se pudieron obtener las localidades");
                });

    }

    return {
        init: init
    };

})();

$(document).on("ready", function () {
    meFaltaUno.ui.registro.init();
});