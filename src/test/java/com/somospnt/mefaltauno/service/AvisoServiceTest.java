package com.somospnt.mefaltauno.service;

import com.somospnt.mefaltauno.ApplicationConfig;
import com.somospnt.mefaltauno.domain.Aviso;
import com.somospnt.mefaltauno.domain.Categoria;
import com.somospnt.mefaltauno.domain.Localidad;
import com.somospnt.mefaltauno.repository.AvisoRepository;
import com.somospnt.mefaltauno.repository.CategoriaRepository;
import com.somospnt.mefaltauno.repository.LocalidadRepository;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfig.class)
public class AvisoServiceTest {

    @Autowired
    private AvisoService avisoService;

    @Autowired
    private AvisoRepository avisoRepository;

    @Autowired
    private LocalidadRepository localidadRepository;

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Test
    public void buscarTodos_conAvisosExistentes_devuelveTodos() {
        List<Aviso> avisos = avisoService.buscarTodos();
        assertTrue(avisos.size() == 1);
        assertTrue(avisos.get(0).getId() == 1l);
        assertEquals(avisos.get(0).getTitulo(), "futbol5");
        assertEquals(avisos.get(0).getDescripcion(), "sarlompa");
        assertTrue(avisos.get(0).getCategoria().getId() == 1);
        assertTrue(avisos.get(0).getLocalidad().getId() == 1);
    }

    @Test
    public void crear_conAvisoCompleto_guardaAviso() {

        Localidad localidadCordoba = localidadRepository.findByNombre("Cordoba");
        Categoria categoriaDeporte = categoriaRepository.findByNombre("deportes");
        Date fechaAviso = new Date();

        Aviso aviso = new Aviso();
        aviso.setId(2L);
        aviso.setTitulo("Gran futbol");
        aviso.setDescripcion("nuevo evento");
        aviso.setCupo(null);
        aviso.setLocalidad(localidadCordoba);
        aviso.setCategoria(categoriaDeporte);
        aviso.setFechaEvento(fechaAviso);

        int avisosAntes = avisoService.buscarTodos().size();
        avisoService.crear(aviso);
        int avisosDespues = avisoService.buscarTodos().size();

        Aviso avisoPersistido = avisoRepository.findOne(2L);

        assertEquals(avisosDespues, avisosAntes + 1);
        assertEquals("Gran futbol", avisoPersistido.getTitulo());
        assertEquals("nuevo evento", avisoPersistido.getDescripcion());
        assertEquals(localidadCordoba.getNombre(), avisoPersistido.getLocalidad().getNombre());
        assertEquals(categoriaDeporte.getNombre(), avisoPersistido.getCategoria().getNombre());
        assertEquals(null, avisoPersistido.getCupo());
        assertEquals(fechaAviso, avisoPersistido.getFechaEvento());

    }

}
