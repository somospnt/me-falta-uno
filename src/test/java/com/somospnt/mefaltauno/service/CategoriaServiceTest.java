package com.somospnt.mefaltauno.service;

import com.somospnt.mefaltauno.ApplicationConfig;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfig.class)
public class CategoriaServiceTest {

    @Autowired
    private CategoriaService categoriaService;

    @Test
    public void obtenerTodas_conCategoriasValidas_obtieneTodasLasCategorias() {

        assertEquals(2, categoriaService.obtenerTodas().size());
    }

}
