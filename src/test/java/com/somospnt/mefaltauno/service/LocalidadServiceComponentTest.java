package com.somospnt.mefaltauno.service;

import com.somospnt.mefaltauno.ApplicationConfig;
import com.somospnt.mefaltauno.domain.Localidad;
import java.util.List;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfig.class)
public class LocalidadServiceComponentTest {

    @Autowired
    private LocalidadService localidadService;

    @Test
    public void obtenerTodas_sinParametros_devuelveListaDeTodasLasProvincias() {
        List<Localidad> localidades = localidadService.obtenerTodas();
        assertNotEquals(localidades.size(), 0);
    }
}
