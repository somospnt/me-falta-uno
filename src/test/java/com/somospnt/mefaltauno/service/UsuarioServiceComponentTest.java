package com.somospnt.mefaltauno.service;

import com.somospnt.mefaltauno.ApplicationConfig;
import com.somospnt.mefaltauno.domain.Usuario;
import com.somospnt.mefaltauno.vo.UsuarioVO;
import java.util.Map;
import javax.sql.DataSource;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfig.class)
public class UsuarioServiceComponentTest {

    @Autowired
    private UsuarioService instance;

    @Autowired
    private DataSource dataSource;

    @Test
    public void buscarPorNombre_ConNombreExistente_DevuelveUsuario() {
        int id = 1;
        String nombre = "Eduardo";
        
        Usuario usuario = instance.buscarPorUsername(nombre);

        assertTrue(usuario.getUsername().equals(nombre));
        assertTrue(usuario.getId() == id);
    }

    @Test
    public void buscarPorNombre_ConNombreInexistente_devuelveNull() {
        String nombre = "Benito";

        Usuario usuario = instance.buscarPorUsername(nombre);

        assertNull(usuario);
    }

    @Test
    public void buscarPorNombre_ConNombreVacio_devuelveNull() {
        String nombre = "";

        Usuario usuario = instance.buscarPorUsername(nombre);

        assertNull(usuario);
    }

    @Test(expected = IllegalArgumentException.class)
    public void buscarPorNombre_ConNombreNull_lanzaExcepcion() {
        String nombre = null;
        instance.buscarPorUsername(nombre);
    }

    @Test(expected = IllegalArgumentException.class)
    public void buscarPorNombre_ConNombreConExpresionesIrregularesComa_lanzaException() {
        String nombre = "nomrbe,";
        instance.buscarPorUsername(nombre);
    }

    @Test(expected = IllegalArgumentException.class)
    public void buscarPorNombre_ConNombreConExpresionesIrregularesSigno_lanzaException() {
        String nombre = "nomr?be";
        instance.buscarPorUsername(nombre);
    }

    @Test(expected = IllegalArgumentException.class)
    public void buscarPorNombre_ConNombreConExpresionesIrregularesParentesis_lanzaException() {
        String nombre = "no(mrb)e";
        instance.buscarPorUsername(nombre);
    }

    @Test
    public void registrar_conNombreValidoYEmailValido_percisteUsuario() {
        UsuarioVO usuarioVO = new UsuarioVO();
        usuarioVO.setNombre("Prueba");
        usuarioVO.setEmail("prueba@prueba.com");
        usuarioVO.setPassword("123456");
        usuarioVO.setNombreLocalidad("Cordoba");

        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource);
        String query = "SELECT * FROM USUARIO WHERE username=?";
        instance.registrar(usuarioVO);
        Map<String, Object> resultados = jdbcTemplate.queryForMap(query, "Prueba");
        assertEquals(usuarioVO.getNombre(), (String) resultados.get("username"));
        assertEquals(usuarioVO.getEmail(), (String) resultados.get("email"));
    }

    
}
