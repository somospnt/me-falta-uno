INSERT INTO PROVINCIA (id, nombre) VALUES (1,'Cordoba');
INSERT INTO PROVINCIA (id, nombre) VALUES (2,'Buenos Aires');

INSERT INTO LOCALIDAD (id, nombre, id_provincia) VALUES (1,'Cordoba',1);
INSERT INTO LOCALIDAD (id, nombre, id_provincia) VALUES (2,'Jesus Maria',1);
INSERT INTO LOCALIDAD (id, nombre, id_provincia) VALUES (3,'Tandil',2);
INSERT INTO LOCALIDAD (id, nombre, id_provincia) VALUES (4,'La Plata',2);

INSERT INTO USUARIO (id, username,password,email,id_localidad, enabled) VALUES (1,'Eduardo','123456','eledu@pnt.com',3, 1);
INSERT INTO USUARIO (id, username,password,email,id_localidad, enabled) VALUES (2,'Francisco','123456','francisco@pnt.com',1, 1);
INSERT INTO USUARIO (id, username,password,email,id_localidad, enabled) VALUES (3,'William','123456','wily@pnt.com',2, 1);
INSERT INTO USUARIO (id, username,password,email,id_localidad, enabled) VALUES (4,'Juan','123456','juan@pnt.com',4, 1);
INSERT INTO USUARIO (id, username,password,email,id_localidad, enabled) VALUES (5,'Ivan','123456','ivan@pnt.com',2, 1);

INSERT INTO CATEGORIA (id, nombre) VALUES (1, 'deportes');
INSERT INTO CATEGORIA (id, nombre) VALUES (2, 'juegos de mesa');

INSERT INTO AVISO (id, titulo, descripcion, fecha_evento, id_categoria, id_localidad) VALUES (1, 'futbol5', 'sarlompa', '2015-07-24 00:00:00', 1, 1);

INSERT INTO USUARIO_AVISO (id_aviso, id_usuario, administrador) VALUES (1, 1, 1);



